package com.tousdan.scraper.task.scrape.summary;

import com.tousdan.scraper.model.Expression;
import com.tousdan.scraper.model.ExpressionResult;
import com.tousdan.scraper.task.scrape.datasource.ScrapeDataSource;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.io.InputStream;
import java.util.Collection;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class KijijiScrapeSummaryTaskTest {

    @Test
    public void givenAKijijiSummaryUrl_whenExtractingAdId_thenExpectedIdIsReturned() {
        String url
            = "http://m.kijiji.ca/portables/ville-de-montreal/lenovo-thinkpad-t410s-slim-laptop-14-core-i5-128gb-ssd/v?adId=1086682057&ck=CK&from=Search&ts=1437939765973";

        assertEquals(Long.valueOf(1086682057), KijijiScrapeSummaryTask.extractAdId(url).get());
    }

    @Test
    public void givenNotAnUrl_whenExtractingAdId_thenReturnsNull() {
        assertFalse(KijijiScrapeSummaryTask.extractAdId("not-an-url").isPresent());
    }

    @Test
    public void givenAKijijiSummaryUrlWithoutAdId_whenExtractingAdId_thenExpectedIdIsReturned() {
        String url
                = "http://m.kijiji.ca/portables/ville-de-montreal/lenovo-thinkpad-t410s-slim-laptop-14-core-i5-128gb-ssd/v?ck=CK&from=Search&ts=1437939765973";

        assertFalse(KijijiScrapeSummaryTask.extractAdId(url).isPresent());
    }

    @Test
    public void givenAKijijiPrice_whenParsingFromStringToInt_thenIsConvertedCorrectly() {
        assertEquals(Integer.valueOf(20), KijijiScrapeSummaryTask.parsePrice("   20 $   ").get());
    }

    @Test
    public void givenAnStringWithNoProice_whenParsingFromStringToInt_thenIsConvertedCorrectly() {
        assertFalse(KijijiScrapeSummaryTask.parsePrice(" Sur Demande   ").isPresent());
    }

    @Test
    public void givenASamplePage_whenScraped_thenTheRightPricesAreExtracted() throws Exception {
        Collection<ExpressionResult> results = givenASample("samples/crf150f-price.html");

        assertThat(results.size(), is(5));

        assertThat(results, contains(resultWithPrice(2650),
                resultWithPrice(4479),
                resultWithPrice(3300),
                resultWithPrice(1800),
                resultWithPrice(2700)));
    }

    @Test
    public void givenASamplePage_whenScraped_thenTheRightTitlesAreExtracted() throws Exception {
        Collection<ExpressionResult> results = givenASample("samples/crf150f-price.html");

        assertThat(results, contains(resultWithTitle("150 f starteur démarre en 2 seconde batterie neuve garantie"),
                resultWithTitle("2015 Honda CRF150F"),
                resultWithTitle("MOTOCROSS HONDA CRF150F"),
                resultWithTitle("CRF 150F 2004"),
                resultWithTitle("Honda CRF 150F")));
    }

    @Test
    public void givenASamplePage_whenScraped_thenTheRightIdsAreExtracted() throws Exception {
        Collection<ExpressionResult> results = givenASample("samples/crf150f-price.html");

        assertThat(results, contains(resultWithId(1090729385),
                resultWithId(1082733208),
                resultWithId(1032399114),
                resultWithId(1086751852),
                resultWithId(1070573300)));
    }

    private Collection<ExpressionResult> givenASample(String url) throws Exception {
        InputStream is = KijijiScrapeSummaryTaskTest.class.getClassLoader().getResourceAsStream(url);

        return new KijijiScrapeSummaryTask(new Expression()).execute(new ScrapeDataSource(is, "http://www.kijiji.ca"));
    }

    private Matcher<ExpressionResult> resultWithPrice(int price) {
        return new TypeSafeMatcher<ExpressionResult>() {
            @Override
            protected boolean matchesSafely(ExpressionResult expressionResult) {
                return expressionResult.price == price;
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(price);
            }
        };
    }

    private Matcher<ExpressionResult> resultWithTitle(String title) {
        return new TypeSafeMatcher<ExpressionResult>() {
            @Override
            protected boolean matchesSafely(ExpressionResult expressionResult) {
                return title.equals(expressionResult.title);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(title);
            }
        };
    }

    private Matcher<ExpressionResult> resultWithId(long id) {
        return new TypeSafeMatcher<ExpressionResult>() {
            @Override
            protected boolean matchesSafely(ExpressionResult expressionResult) {
                return id == expressionResult.providerId;
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(id);
            }
        };
    }
}