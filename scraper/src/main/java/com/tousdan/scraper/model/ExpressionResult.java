package com.tousdan.scraper.model;

/**
 * Created by tousdan on 26/07/15.
 */
public class ExpressionResult {
    public final int expression;
    public final Long providerId;
    public final Integer price;
    public final String title;
    public final String detailUrl;

    public ExpressionResult(int expression, Long providerId,Integer price, String title, String detailUrl) {
        this.expression = expression;
        this.price = price;
        this.title = title;
        this.detailUrl = detailUrl;
        this.providerId = providerId;
    }

    @Override
    public String toString() {
        return "ExpressionResult{" +
                "expression=" + expression +
                ", providerId=" + providerId +
                ", price=" + price +
                ", title='" + title + '\'' +
                ", detailUrl='" + detailUrl + '\'' +
                '}';
    }
}
