package com.tousdan.scraper.worker.scrape.summary;

import com.rabbitmq.client.*;
import com.tousdan.scraper.Scraper;
import com.tousdan.scraper.exception.MQSetupException;
import com.tousdan.scraper.model.Expression;
import com.tousdan.scraper.model.ExpressionResult;
import com.tousdan.scraper.task.scrape.datasource.ScrapeDataSource;
import com.tousdan.scraper.task.scrape.summary.KijijiScrapeSummaryTask;
import com.tousdan.scraper.util.MQMessageParsingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.sql.Connection;
import java.util.Collection;

public class AdExpressionSummaryConsumer extends DefaultConsumer {
    private static final Logger LOG = LoggerFactory.getLogger(AdExpressionSummaryConsumer.class);
    private final Connection sqlConnection;
    private final Scraper.Config config;

    public AdExpressionSummaryConsumer(Channel channel, Connection sqlConnection, Scraper.Config config) {
        super(channel);

        this.sqlConnection = sqlConnection;
        this.config = config;
    }

    public AdExpressionSummaryConsumer setUp() throws MQSetupException {
        try {
            getChannel().queueDeclare("scrape-expression", true, false, false, null);
            getChannel().basicConsume("scrape-expression", false, this);
        } catch (IOException io) {
            throw new MQSetupException(io);
        }

        return this;
    }

    public boolean isAlive() {
        return getChannel().isOpen();
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        long deliveryTag = envelope.getDeliveryTag();

        try {
            Expression expression = MQMessageParsingUtil.parse(properties, body, Expression.class);

            LOG.info("Received a new expression scraping message [id:{}]", expression.getId());

            KijijiScrapeSummaryTask task = null;
            if ("kijiji".equals(expression.getSite())) {
                task = new KijijiScrapeSummaryTask(expression);
            }

            if (task == null) {
                LOG.warn("Received task of unknown adSite, ignoring");
                getChannel().basicNack(deliveryTag, false, false);
            } else {
                LOG.debug("Executing task");
                ScrapeDataSource dataSource
                        = new ScrapeDataSource(expression.getQueryURL());

                config.saveRequestIfRequired(dataSource, expression);

                Collection<ExpressionResult> results = task.execute(dataSource);

                long added
                    = results.stream().filter(e -> saveExpressionResult(expression, e)).count();

                LOG.debug("Added {} new results for expression {} ({} duplicates were ignored)", added, expression.getId(), results.size() - added);
                getChannel().basicAck(deliveryTag, false);

            }
        } catch (Exception ex) {
            LOG.error("Could not process delivery", ex);

            throw ex;
        }
    }

    private boolean saveExpressionResult(Expression expression, ExpressionResult result) {
        boolean exists = false;
        try {
            if (result.providerId != null ) {
                PreparedStatement queryStatement
                        = sqlConnection.prepareStatement("SELECT providerId FROM ad_results WHERE providerId = ? AND expression = ?");

                queryStatement.setLong(1, result.providerId);
                queryStatement.setInt(2, expression.getId());

                exists = queryStatement.executeQuery().next();
            }

            if (!exists) {
                PreparedStatement insertStatement
                        = sqlConnection.prepareStatement("INSERT INTO ad_results (expression, author, url, price, title, providerId) VALUES (?, ?, ?, ?, ?, ?)");

                insertStatement.setInt(1, expression.getId());
                insertStatement.setInt(2, -1);
                insertStatement.setString(3, result.detailUrl);

                if (result.price == null) {
                    insertStatement.setNull(4, Types.INTEGER);
                } else {
                    insertStatement.setInt(4, result.price);
                }

                insertStatement.setString(5, result.title);

                if (result.providerId == null) {
                    insertStatement.setNull(6, Types.BIGINT);
                } else {
                    insertStatement.setLong(6, result.providerId);
                }

                insertStatement.execute();
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return !exists;
    }
}
