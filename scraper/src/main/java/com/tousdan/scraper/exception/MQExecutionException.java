package com.tousdan.scraper.exception;

public class MQExecutionException extends RuntimeException {
    public MQExecutionException(Throwable cause) {
        super(cause);
    }
}
