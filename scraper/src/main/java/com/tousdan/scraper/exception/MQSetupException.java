package com.tousdan.scraper.exception;

public class MQSetupException extends RuntimeException {
    public MQSetupException(Throwable cause) {
        super(cause);
    }
}
