package com.tousdan.scraper.util;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.QueueingConsumer;

/**
 * Created by tousdan on 24/07/15.
 */
public class MQMessageParsingUtil {
    private static final Gson gson = new Gson();

    public static <T> T parse(AMQP.BasicProperties properties, byte[] data, Class<T> klass) {
        String contentType = properties.getContentType();

        if ("application/json".equals(contentType)) {
            return gson.fromJson(new String(data), klass);
        } else {
            throw new IllegalArgumentException(String.format("Cannot parse content type : ", contentType));
        }
    }
}
