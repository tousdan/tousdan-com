package com.tousdan.scraper;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.rabbitmq.client.*;
import com.tousdan.scraper.model.Expression;
import com.tousdan.scraper.task.scrape.datasource.ScrapeDataSource;
import com.tousdan.scraper.task.scrape.datasource.ScrapeDatasourceSaver;
import com.tousdan.scraper.worker.scrape.summary.AdExpressionSummaryConsumer;
import org.jsoup.helper.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by tousdan on 24/07/15.
 */
public class Scraper {

    private static final Logger LOG = LoggerFactory.getLogger(Scraper.class);

    public static void main(String[] args) throws Exception {
        new Scraper().run();
    }

    private static final int AMOUNT_OF_WORKERS = 1;

    private Connection rabbitConnection;
    private java.sql.Connection sqlConnection;
    private Config config;

    public void run() throws Exception {
        ConnectionFactory fact = configureRabbitConnectionFactory();
        rabbitConnection = fact.newConnection();
        config = new Config();
        sqlConnection = configureDataSource().getConnection();

        List<AdExpressionSummaryConsumer> workers = new ArrayList<>();

        while (true) {
            workers = spawnWorkers(workers.parallelStream().filter(AdExpressionSummaryConsumer::isAlive));
            Thread.sleep(5000);
        }
    }

    private List<AdExpressionSummaryConsumer> spawnWorkers(Stream<AdExpressionSummaryConsumer> currentWorkers) {
        List<AdExpressionSummaryConsumer> consumers = currentWorkers.collect(Collectors.toList());

        int diff = AMOUNT_OF_WORKERS - consumers.size();

        if (diff > 0) {
            LOG.error("Some of the consumers were dead :: spawning %s consumers", diff);
            consumers.addAll(IntStream.range(0, diff)
                    .boxed()
                    .map(i -> new AdExpressionSummaryConsumer(prepareChannel(rabbitConnection), sqlConnection, config).setUp())
                    .collect(Collectors.toList()));
        }

        return consumers;
    }

    private Channel prepareChannel(Connection connection) {
        try {
            Channel channel = connection.createChannel();

            channel.basicQos(1);

            return channel;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private ConnectionFactory configureRabbitConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("AMQP_HOST"));
        factory.setAutomaticRecoveryEnabled(true);

        return factory;
    }

    private DataSource configureDataSource() {
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        cpds.setJdbcUrl(System.getenv("DATABASE_URL"));

        String username = System.getenv("DATABASE_USER");
        String password = System.getenv("DATABASE_PASSWORD");

        if (username != null && username.length() > 0) { cpds.setUser(username); }
        if (password != null && password.length() > 0) { cpds.setPassword(password); }

        return cpds;
    }

    public static class Config {
        final boolean saveRequests;
        final Path savePath;

        public Config() {
            String save = System.getenv("SAVE_REQUESTS");
            String savePath = System.getenv("SAVE_PATH");
            Path savePathLocation = Paths.get(savePath);

            if ("true".equalsIgnoreCase(save) && !StringUtil.isBlank(savePath)) {
                if (!Files.exists(savePathLocation)) {
                    LOG.error("Configuration wanted to save requests to {} but path does not exist", savePath);
                    this.saveRequests = false;
                    this.savePath = null;
                } else {
                    this.saveRequests = true;
                    this.savePath = savePathLocation;
                }
            } else {
                this.saveRequests = false;
                this.savePath = null;
            }
        }

        public void saveRequestIfRequired(ScrapeDataSource dataSource, Expression ex) {
            String expression = Integer.toString(ex.getId());
            String uuid = UUID.randomUUID().toString();
            File filePath = savePath.resolve(expression).resolve(uuid).toFile();

            if (new ScrapeDatasourceSaver(filePath).save(dataSource)) {
                LOG.debug("Saved request for expression %s @ %s", ex.getId(), filePath);
            } else {
                LOG.error("Could not save request for expression %s @ %s", ex.getId(), filePath);
            }

        }
    }
}
