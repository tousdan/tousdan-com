package com.tousdan.scraper.task.scrape.datasource;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by tousdan on 28/07/15.
 */
public class ScrapeDataSource {
    public final Document document;

    public ScrapeDataSource(String URL) throws IOException {
        this.document = Jsoup.connect(URL).get();
    }
    
    public ScrapeDataSource(InputStream is, String baseUri) throws IOException {
        this.document = Jsoup.parse(is, "UTF-8", baseUri);
    }
}
