package com.tousdan.scraper.task.scrape.summary;

import com.tousdan.scraper.model.Expression;
import com.tousdan.scraper.model.ExpressionResult;
import com.tousdan.scraper.task.scrape.datasource.ScrapeDataSource;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.text.html.Option;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KijijiScrapeSummaryTask {
    private static final Logger LOG = LoggerFactory.getLogger(KijijiScrapeSummaryTask.class);

    private final Expression expression;

    public KijijiScrapeSummaryTask(Expression expression) {
        this.expression = expression;
    }

    private Collection<ExpressionResult> parseModeOne(Elements regular_ads) {
        Collection<ExpressionResult> results = new ArrayList<>();

        for(Element element : regular_ads) {
            String link = element.getElementById("itemLink").attr("href");
            String price = element.getElementById("adPriceDiv").text();
            String title = element.getElementById("adTitleDiv").text();

            Optional<Long> adId = extractAdId(link);

            if (!adId.isPresent()) {
                LOG.warn("Received an url with no adId when processing expression @ {} ({})", expression.getQueryURL(), link);
            }

            results.add(new ExpressionResult(expression.getId(), adId.orElse(null), parsePrice(price).orElse(null), title, link));
        }

        return results;
    }

    private Collection<ExpressionResult> parseModeTwo(Elements regular_ads) {
        Collection<ExpressionResult> results = new ArrayList<>();

        for(Element element : regular_ads) {
            String link = element.attr("data-vip-url");
            String price = element.select(".price").text();
            String title = element.select(".title").text();

            Long adId = Long.parseLong(element.select(".watch").attr("data-adid"));

            results.add(new ExpressionResult(expression.getId(), adId, parsePrice(price).orElse(null), title, link));
        }

        return results;
    }

    public Collection<ExpressionResult> execute(ScrapeDataSource dataSource) {
        Document parseResult = dataSource.document;

        List<ExpressionResult> results = new ArrayList<>();

        Elements regular_ads = parseResult.select(".adTopLevel");

        if (regular_ads.size() != 0) {
            results.addAll(parseModeOne(regular_ads));
        } else {
            regular_ads = parseResult.select(".regular-ad");

            if(regular_ads.size() != 0) {
                results.addAll(parseModeTwo(regular_ads));
            } else {
                LOG.error("Was unable to parse with known formulas {}", expression.getQueryURL());
            }
        }

        LOG.info(String.format("Parsed %s results", results.size()));

        return results;
    }

    private static final Pattern pricePattern = Pattern.compile("(\\d)+");

    public static Optional<Integer> parsePrice(String price) {
        Matcher m = pricePattern.matcher(price.replaceAll("\\s", ""));

        if (m.find()) {
            return Optional.of(Integer.parseInt(m.group()));
        }

        return Optional.empty();
    }

    public static Optional<Long> extractAdId(String url) {
        try {
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");

            Optional<NameValuePair> adId = params.stream().filter(kv -> "adId".equals(kv.getName())).findFirst();

            if (adId.isPresent()) {
                return Optional.of(Long.valueOf(adId.get().getValue()));
            }
        } catch (URISyntaxException uri) {}

        return Optional.empty();
    }
}
