package com.tousdan.scraper.task.scrape.datasource;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by tousdan on 17/08/15.
 */
public class ScrapeDatasourceSaver {
    private final File path;

    public ScrapeDatasourceSaver(File path) {
        this.path = path;
    }

    public boolean save(ScrapeDataSource dataSource) {
        try {
            FileUtils.writeStringToFile(path, dataSource.document.outerHtml(), "UTF-8");

            return true;
        } catch (IOException ioe) {
            return false;
        }
    }
}
