package com.tousdan.di;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import javax.sql.DataSource;
import java.io.IOException;

public class DIModule extends AbstractModule {

    private final DataSource datasSource;
    private final ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;

    public DIModule(DataSource dataSource, ConnectionFactory connectionFactory) {
        this.datasSource = dataSource;
        this.connectionFactory = connectionFactory;
    }

    @Override
    protected void configure() {
        try {
            this.connection = connectionFactory.newConnection();
            this.channel = connection.createChannel();
        } catch (IOException ioe) {
            throw new RuntimeException("Couldnt establish connection to rabbit", ioe);
        }

        bind(DataSource.class).toInstance(this.datasSource);
        bind(Channel.class).toInstance(this.channel);
    }
}
