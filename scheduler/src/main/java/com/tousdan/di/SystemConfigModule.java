package com.tousdan.di;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**
 * Created by tousdan on 30/06/15.
 */
public class SystemConfigModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Integer.class)
                .annotatedWith(Names.named("refresh-amount"))
                .toInstance(5);
    }
}
