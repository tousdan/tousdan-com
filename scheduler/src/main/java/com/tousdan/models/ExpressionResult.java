package com.tousdan.models;

/**
 * Created by tousdan on 30/06/15.
 */
public class ExpressionResult {
    public final int expression;
    public final String price;
    public final String title;
    public final String detailUrl;

    public ExpressionResult(int expression, String price, String title, String detailUrl) {
        this.expression = expression;
        this.price = price;
        this.title = title;
        this.detailUrl = detailUrl;
    }
}
