package com.tousdan.models;

public class Expression {
    public final int id;
    public final String queryURL;
    public final String site;

    public Expression(int id, String queryURL, String site) {
        this.id = id;
        this.queryURL = queryURL;
        this.site = site;
    }

    @Override
    public String toString() {
        return "Expression{" +
                "id=" + id +
                ", queryURL='" + queryURL + '\'' +
                '}';
    }
}
