package com.tousdan;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.rabbitmq.client.ConnectionFactory;
import com.tousdan.di.DIModule;
import com.tousdan.di.SystemConfigModule;
import com.tousdan.quartz.jobfactory.GuavaJobFactory;
import com.tousdan.quartz.jobs.RefreshExpressionJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

public class SchedulerApp
{
    private static final Logger LOG = LoggerFactory.getLogger(SchedulerApp.class);

    private final Injector injector;

    public SchedulerApp() {
        DataSource ds = configurePooledDataSource();
        ConnectionFactory cf = configureRabbitConnectionFactory();

        this.injector = configureInjector(ds, cf);
    }

    public void run() {
        try {
            Scheduler scheduler = configureScheduler();

            scheduler.start();

            try {
                while (true) {
                    Thread.sleep(1000);
                }
            } catch (InterruptedException ie) {
                LOG.error("Interrupted while sleeping", ie);
            }

            scheduler.shutdown();
        } catch (SchedulerException sex) {
            LOG.error("Scheduling error", sex);

            throw new RuntimeException(sex);
        }
    }

    private Injector configureInjector(DataSource dataSource, ConnectionFactory amqpFactory) {
        return Guice.createInjector(
                new DIModule(dataSource, amqpFactory),
                new SystemConfigModule());
    }

    private Scheduler configureScheduler() throws SchedulerException {
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler scheduler = schedulerFactory.getScheduler();

        JobDetail refreshExpressions = JobBuilder.newJob(RefreshExpressionJob.class)
                .withIdentity("RefreshExpressions", "expressions-sched")
                .build();

        Trigger halfHour = TriggerBuilder.newTrigger()
                .withIdentity("Half-hours", "expressions-sched")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                    .withIntervalInMinutes(30)
                    .repeatForever())
                .build();

        scheduler.scheduleJob(refreshExpressions, halfHour);

        scheduler.setJobFactory(new GuavaJobFactory(injector));

        return scheduler;
    }

    private DataSource configurePooledDataSource() {
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        cpds.setJdbcUrl(System.getenv("DATABASE_URL"));

        String username = System.getenv("DATABASE_USER");
        String password = System.getenv("DATABASE_PASSWORD");

        if (username != null && username.length() > 0) { cpds.setUser(username); }
        if (password != null && password.length() > 0) { cpds.setPassword(password); }

        return cpds;
    }

    private ConnectionFactory configureRabbitConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("AMQP_HOST"));
        factory.setAutomaticRecoveryEnabled(true);
        return factory;
    }

    public static void main( String[] args ) {
        new SchedulerApp().run();
    }
}
