package com.tousdan.quartz.jobs;

import com.tousdan.traits.AMQPClosable;
import org.quartz.Job;

public interface SchedulerJob extends Job, AMQPClosable {
}
