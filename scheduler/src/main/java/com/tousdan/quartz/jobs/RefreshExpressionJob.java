package com.tousdan.quartz.jobs;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.rabbitmq.client.*;
import com.tousdan.constants.Queues;
import com.tousdan.models.Expression;
import org.apache.commons.dbutils.QueryRunner;
import org.jsoup.Jsoup;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.List;


public class RefreshExpressionJob implements SchedulerJob {

    private final static Logger LOG = LoggerFactory.getLogger(RefreshExpressionJob.class);

    private final DataSource dataSource;
    private final int amountToRefresh;
    private final Channel channel;

    private static final String FETCH_QUERY = "SELECT site, query, id FROM ad_expressions ORDER BY created LIMIT ?";

    private AMQP.BasicProperties basicProperties;
    @Inject
    public RefreshExpressionJob(DataSource dataSource, Channel channel, @Named("refresh-amount") int amountToRefresh) {
        this.channel = channel;
        this.amountToRefresh = amountToRefresh;
        this.dataSource = dataSource;

        this.basicProperties = new AMQP.BasicProperties.Builder().contentType("application/json").build();

        try {
            channel.queueDeclare("scrape-expression", true, false, false, null);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private List<Expression> fetchExpressionsToRefresh() {
        try {
            return new QueryRunner(dataSource).query(FETCH_QUERY, (rs) -> {
                List<Expression> exprs = Lists.newArrayList();

                while (rs.next()) {
                    exprs.add(new Expression(rs.getInt("id"), rs.getString("query"), rs.getString("site")));
                }

                return exprs;
            }, this.amountToRefresh);
        } catch (SQLException sqe) {
            throw new RuntimeException("Could not execute expressions fetch query", sqe);
        }
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<Expression> expressions = fetchExpressionsToRefresh();

        if (!expressions.isEmpty()) {
            LOG.debug(String.format("Processing %s expressions", expressions.size()));

            try {
                for (Expression ex : expressions) {
                    channel.basicPublish("", "scrape-expression", basicProperties, new Gson().toJson(ex).getBytes(Charset.forName("UTF-8")));
                }
            } catch (Exception e) {
                LOG.error("Could not refresh jobs", e);
                throw new JobExecutionException(true);
            }
        }
    }
}
