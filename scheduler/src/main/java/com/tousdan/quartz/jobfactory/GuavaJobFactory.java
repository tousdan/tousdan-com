package com.tousdan.quartz.jobfactory;

import com.google.inject.Injector;
import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

public class GuavaJobFactory implements JobFactory {
    private final Injector injector;

    public GuavaJobFactory(Injector injector) {
        this.injector = injector;
    }

    public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
        Class<? extends Job> jobClass = bundle.getJobDetail().getJobClass();

        try {
            return injector.getInstance(jobClass);
        } catch (Exception ex) {
            throw new SchedulerException("Could not instantiate instance of class " + jobClass.toString(), ex);
        }
    }
}
