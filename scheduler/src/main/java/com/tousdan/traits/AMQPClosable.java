package com.tousdan.traits;

import com.rabbitmq.client.Channel;

public interface AMQPClosable {
    default void closeQuietly(Channel channel) {
        try {
            channel.close();
        } catch (Exception e) {
            //shhh!
        }
    }
}
