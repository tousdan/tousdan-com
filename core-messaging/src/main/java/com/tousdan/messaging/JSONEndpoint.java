package com.tousdan.messaging;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import java.nio.charset.Charset;

/**
 * Created by tousdan on 25/07/15.
 */
public class JSONEndpoint extends MessagingEndpoint {
    private static final Charset CHARSET = Charset.forName("UTF-8");
    private static final AMQP.BasicProperties JSON_CONTENT_TYPE
            = new AMQP.BasicProperties.Builder()
                .contentType("application/json")
                .build();

    private final Gson serializer;

    public JSONEndpoint(Gson serializer, QueueBuilder options, Channel channel) {
        super(options, channel);

        this.serializer = serializer;
    }

    public void publish(Object data) {
        byte[] asBytes = serializer.toJson(data).getBytes(CHARSET);

        this.publish(JSON_CONTENT_TYPE, asBytes);
    }
}
