package com.tousdan.messaging;

import com.google.inject.Inject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.tousdan.messaging.exception.EndpointSetupException;
import com.tousdan.messaging.exception.MessageUndeliveredException;

import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.util.Map;

public abstract class MessagingEndpoint {
    protected final Channel channel;
    protected final String endPointName;

    @Inject
    public MessagingEndpoint(QueueBuilder options, Channel channel) {
        this.endPointName = options.queueName;
        this.channel = channel;

        options.setUp(channel);
    }

    protected void publish(AMQP.BasicProperties properties, byte[] data) throws MessageUndeliveredException {
        try {
            this.channel.basicPublish("", endPointName, properties, data);
        } catch (IOException ioe) {
            throw new MessageUndeliveredException(this.endPointName, data, ioe);
        }
    }

    public static class QueueBuilder {
        private final String queueName;

        private boolean durable;
        private boolean exclusive;
        private boolean autoDelete;

        public QueueBuilder(String queueName) {
            this.queueName = queueName;

            this.durable = false;
            this.exclusive = false;
            this.autoDelete = true;
        }

        public void setUp(Channel channel) {
            try {
                channel.queueDeclare(queueName, durable, exclusive, autoDelete, null);
            } catch (IOException ioe) {
                throw new EndpointSetupException("Could not setup endpoint \"" + queueName + "\"", ioe);
            }
        }

        public QueueBuilder durable(boolean durable) {
            this.durable = durable;
            return this;
        }

        public QueueBuilder exclusive(boolean exclusive) {
            this.exclusive = exclusive;
            return this;
        }

        public QueueBuilder autoDelete(boolean autoDelete) {
            this.autoDelete = autoDelete;
            return this;
        }
    }
}
