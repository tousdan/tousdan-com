package com.tousdan.messaging.exception;

public class EndpointSetupException extends RuntimeException {
    public EndpointSetupException(String message, Throwable cause) {
        super(message, cause);
    }
}
