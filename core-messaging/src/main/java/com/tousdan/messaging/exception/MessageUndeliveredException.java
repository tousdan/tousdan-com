package com.tousdan.messaging.exception;

/**
 * Created by tousdan on 25/07/15.
 */
public class MessageUndeliveredException extends RuntimeException {
    private final byte[] data;
    private final String queue;

    public MessageUndeliveredException(String queue, byte[] data, Exception realCause) {
        super("Could not publish data", realCause);
        this.data = data;
        this.queue = queue;
    }

    public byte[] getData() {
        return data;
    }

    public String getQueue() {
        return queue;
    }
}
