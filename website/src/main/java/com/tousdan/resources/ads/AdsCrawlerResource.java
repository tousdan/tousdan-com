package com.tousdan.resources.ads;

import com.tousdan.models.ads.AdExpression;
import com.tousdan.models.ads.AdResult;
import com.tousdan.resources.Resource;
import com.tousdan.services.ads.AdsExpressionService;
import com.tousdan.util.FTLUtil;
import com.tousdan.util.RoutesUtil;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

import javax.inject.Inject;
import java.util.Collection;

import static spark.Spark.*;

public class AdsCrawlerResource implements Resource {

    private final AdsExpressionService expressionService;
    private final TemplateEngine templateEngine;

    @Inject
    public AdsCrawlerResource(TemplateEngine templateEngine, AdsExpressionService expressionService) {
        this.expressionService = expressionService;
        this.templateEngine = templateEngine;
    }

    @Override
    public void register() {
        before("ads/*", RoutesUtil.mustBeLoggedIn());

        get("ads/expressions", this::listExpressions, templateEngine);
        get("ads/expressions/new", this::newExpression, templateEngine);
        get("ads/expressions/:id", this::showExpression, templateEngine);
    }

    private ModelAndView listExpressions(Request request, Response response) {
        return FTLUtil
            .getBaseModel("ads.expressions/list.ftl", request)
            .add("expressions", expressionService.listExpressions());
    }

    private ModelAndView newExpression(Request request, Response response) {
        return FTLUtil
                .getBaseModel("ads.expressions/new.ftl", request);
    }

    private ModelAndView showExpression(Request request, Response response) {
        int expressionId = Integer.parseInt(request.params("id"));
        AdExpression expression = expressionService.getExpression(expressionId);
        Collection<AdResult> results = expressionService.listResults(expressionId);

        return FTLUtil
                .getBaseModel("ads.expressions/results.ftl", request)
                .add("results", results)
                .add("expression", expression);
    }
}
