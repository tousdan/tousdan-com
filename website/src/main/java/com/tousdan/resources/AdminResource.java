package com.tousdan.resources;

import com.tousdan.models.Roles;
import com.tousdan.models.SessionUser;
import com.tousdan.services.UserManagementService;
import com.tousdan.services.UserPermissionsService;
import com.tousdan.util.FTLUtil;
import com.tousdan.util.RoutesUtil;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

import javax.inject.Inject;

import java.util.HashMap;

import static spark.Spark.*;


public class AdminResource implements Resource {

    private final TemplateEngine templateEngine;
    private final UserManagementService userManagementService;
    private final UserPermissionsService permissionsService;

    @Inject
    public AdminResource(TemplateEngine templateEngine, UserManagementService userManagementService, UserPermissionsService permissionsService) {
        this.userManagementService = userManagementService;
        this.permissionsService = permissionsService;
        this.templateEngine = templateEngine;
    }

    @Override
    public void register() {
        get("a", this::home, templateEngine);
        get("a/users", this::listUsers, templateEngine);
        get("a/users/new", this::addUser, templateEngine);


        before("a", RoutesUtil.failUnlessHasRole(permissionsService, Roles.admin));
        before("a/*", RoutesUtil.failUnlessHasRole(permissionsService, Roles.admin));
    }

    private ModelAndView home(Request req, Response res) {
        return FTLUtil.getBaseModel("admin/home.ftl", req);
    }

    private ModelAndView listUsers(Request req, Response res) {
        return FTLUtil.getBaseModel("admin/user.list.ftl", req)
                .add("users", userManagementService.getUsers(new SessionUser(req)));
    }

    private ModelAndView addUser(Request req, Response res) {
        return FTLUtil.getBaseModel("admin/user.add.ftl", req)
                .add("roles", Roles.values());
    }
}
