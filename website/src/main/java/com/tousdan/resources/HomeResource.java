package com.tousdan.resources;

import com.tousdan.util.FTLUtil;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

import javax.inject.Inject;

import static spark.Spark.get;

/**
 * Created by tousdan on 27/07/15.
 */
public class HomeResource implements Resource {
    private final TemplateEngine templateEngine;

    @Inject
    public HomeResource(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public void register() {
        get("/home", this::homePage, templateEngine);
        get("/", this::homePage, templateEngine);
    }

    private ModelAndView homePage(Request request, Response response) {
        return FTLUtil.getBaseModel("home.ftl", request);
    }
}
