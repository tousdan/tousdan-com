package com.tousdan.resources;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.tousdan.data.models.tables.records.KeywordLinksRecord;
import com.tousdan.services.KeywordLinksService;
import com.tousdan.services.UserPermissionsService;
import com.tousdan.util.FTLUtil;
import freemarker.cache.WebappTemplateLoader;
import org.apache.commons.validator.routines.UrlValidator;
import org.jooq.exception.DataAccessException;
import spark.*;
import spark.template.freemarker.FreeMarkerEngine;

import static spark.Spark.*;

import java.util.HashMap;
import java.util.Map;

public class KeywordRedirectResource implements Resource {
    private final Gson gson;
    private final UrlValidator urlValidator;
    private final UserPermissionsService permissions;
    private final KeywordLinksService links;
    private final TemplateEngine templateEngine;

    @Inject
    public KeywordRedirectResource(TemplateEngine templateEngine, UserPermissionsService permissions, KeywordLinksService links) {
        this.gson = new Gson();
        this.urlValidator = new UrlValidator(new String[] { "http", "https" });

        this.permissions = permissions;
        this.templateEngine = templateEngine;
        this.links = links;
    }

    public void register() {
        get("/k/add", this::addKeywordForm, templateEngine);
        post("/k/add", this::addKeyword, gson::toJson);
        get("/k/:keyword", this::getKeyword);

        before("/k/add", (req, res) -> {
            if (!permissions.isAuthenticated(req)) {
                halt(403);
            }
        });
    }

    private Object addKeyword(Request req, Response res) {
        String keyword = req.queryParams("keyword");
        String url = req.queryParams("url");

        try {
            links.addKeyword(keyword, url);
        } catch (DataAccessException dae) {
            res.status(500);

            return new Error("Could not create keyword - already in use");
        }

        return new Keyword(keyword, url);
    }

    private static class Error {
        public final String error;

        public Error(String error) {
            this.error = error;
        }
    }
    private static class Keyword {
        public final String keyword;
        public final String url;

        public Keyword(String keyword, String url) {
            this.keyword = keyword;
            this.url = url;
        }
    }

    private String getKeyword(Request req, Response res) {
        KeywordLinksRecord keyword = links.getKeyword(req.params("keyword"));

        if (keyword == null) {
            res.status(404);
        } else {
            res.redirect(keyword.getUrl());
        }

        return "";
    }

    private ModelAndView addKeywordForm(Request req, Response res) {
        FTLUtil.WebAppModelView mv = FTLUtil.getBaseModel("add_keyword.ftl", req);

        mv.add("keyword", "");
        mv.add("url", "");

        return mv;
    }
}
