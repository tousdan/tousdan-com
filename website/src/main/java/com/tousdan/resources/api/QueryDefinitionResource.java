package com.tousdan.resources.api;

import com.tousdan.models.SessionUser;
import com.tousdan.models.queries.GamePriceQueryDefinition;
import com.tousdan.models.queries.QueryDefinition;
import com.tousdan.services.queries.QueryDefinitionService;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by tousdan on 10/08/16.
 */
public class QueryDefinitionResource implements JSONResource {
    private final QueryDefinitionService service;

    @Inject
    public QueryDefinitionResource(QueryDefinitionService service) {
        this.service = service;
    }

    @Override
    public void register() {
        post("queries", this::addQuery);
        get("queries", this::listQueries);
    }

    private Collection<QueryDefinition> listQueries(Request req, Response res) {
        return service.listQueriesForUser(getUserId(req)).collect(Collectors.toList());
    }

    private QueryDefinition addQuery(Request req, Response res) {
        QueryParamsMap queryMap = req.queryMap("query");

        if (!queryMap.get("type").hasValue()) {
           throw new IllegalArgumentException("query.type must be provided");
        }

        QueryDefinition.QueryType type = QueryDefinition.QueryType.valueOf(queryMap.get("type").value());

        final QueryDefinition query;

        if (QueryDefinition.QueryType.GamePrice.equals(type)) {
            query = new GamePriceQueryDefinition(queryMap.get("name").value());
        } else {
            throw new UnsupportedOperationException("Type is not supported, yet.");
        }

        return service.addQuery(getUserId(req), query);
    }

    private int getUserId(Request req) {
        return new SessionUser(req).getId();
    }
}
