package com.tousdan.resources.api;

import com.tousdan.models.SessionUser;
import com.tousdan.models.ads.AdExpression;
import com.tousdan.models.ads.AdSite;
import com.tousdan.services.ads.AdsExpressionService;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import java.util.Collection;

public class ExpressionResource implements JSONResource<AdExpression> {
    private final AdsExpressionService expressionService;

    @Inject
    public ExpressionResource(AdsExpressionService expressionService) {
        this.expressionService = expressionService;
    }

    @Override
    public void register() {
        post("expressions", this::addExpression);
        get("expressions", this::listExpressions);
        get("expressions/:id/refresh", this::refreshExpression);
    }

    private AdExpression addExpression(Request req, Response response) {
        QueryParamsMap expr = req.queryMap("expr");

        AdExpression adExpression = new AdExpression(
                null,
                AdSite.valueOf(expr.get("site").value()),
                expr.get("description").value(),
                expr.get("query").value()
        );

        int id = expressionService.addExpression(new SessionUser(req), adExpression);

        return expressionService.getExpression(id);
    }

    private Collection<AdExpression> refreshExpression(Request request, Response response) {
        expressionService.refresh(Integer.parseInt(request.params(":id")));

        return null;
    }

    private Collection<AdExpression> listExpressions(Request request, Response response) {
        return expressionService.listExpressions();
    }
}
