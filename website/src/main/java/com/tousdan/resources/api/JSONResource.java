package com.tousdan.resources.api;

import com.google.gson.Gson;
import com.tousdan.resources.Resource;
import spark.Route;
import spark.Spark;

import java.util.Collection;

public interface JSONResource<T> extends Resource {
    default void get(final String path, final JSONRoute<Collection<T>> route) {
        Spark.get(JSONResource.abs_to_rel(path), (req, res) -> {
            res.type("application/json");
            return route.handle(req, res);
        }, new Gson()::toJson);
    }

    default void post(final String path, final JSONRoute<T> route) {
        Spark.post(JSONResource.abs_to_rel(path), (req, res) -> {
            res.type("application/json");
            return route.handle(req, res);
        }, new Gson()::toJson);
    }

    static String abs_to_rel(String url) {
        if(url.startsWith("/")) {
            return "api" + url;
        } else {
            return "api/" + url;
        }
    }
}
