package com.tousdan.resources.api;

import com.tousdan.data.models.tables.records.UsersRecord;
import com.tousdan.models.Roles;
import com.tousdan.models.SessionUser;
import com.tousdan.models.User;
import com.tousdan.services.UserManagementService;
import com.tousdan.services.UserPermissionsService;
import com.tousdan.util.RoutesUtil;
import org.jooq.DSLContext;
import spark.Request;
import spark.Response;
import spark.Route;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

import static spark.Spark.before;

public class UsersResource implements JSONResource<User> {
    private final UserPermissionsService permissionsService;
    private final UserManagementService userManagementService;

    @Inject
    public UsersResource(UserPermissionsService permissionsService, UserManagementService userManagementService) {
        this.permissionsService = permissionsService;
        this.userManagementService = userManagementService;
    }


    @Override
    public void register() {
        before("users", RoutesUtil.failUnlessHasRole(permissionsService, Roles.admin));

        get("users", this::listUsers);
        post("users", this::createUser);
    }


    private Collection<User> listUsers(Request req, Response res) {
        return null;
    }

    private User createUser(Request req, Response res) {
        //TODO: parse payload instead of params.
        String username = req.queryParams("user");
        Roles role = Roles.valueOf(req.queryParams("role"));

        return userManagementService.addNewUser(new SessionUser(req), username, role);
    }

}
