package com.tousdan.resources.api;

import spark.Request;
import spark.Response;
import spark.Route;

public interface JSONRoute<T> extends Route {
    T handle(Request request, Response response) throws Exception;
}
