package com.tousdan.resources.queries;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;
import com.tousdan.models.SessionUser;
import com.tousdan.resources.Resource;
import com.tousdan.services.queries.QueryDefinitionService;
import com.tousdan.util.FTLUtil;
import com.tousdan.util.RoutesUtil;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

import javax.inject.Inject;

import java.util.stream.Collectors;

import static spark.Spark.before;
import static spark.Spark.get;

/**
 * Created by tousdan on 17/08/16.
 */
public class QueriesResource implements Resource {

    private final TemplateEngine engine;
    private final QueryDefinitionService service;

    @Inject
    public QueriesResource(TemplateEngine engine, QueryDefinitionService service) {
        this.engine = engine;
        this.service = service;
    }

    @Override
    public void register() {
        before("queries/*", RoutesUtil.mustBeLoggedIn());

        get("queries/list", this::list, engine);
    }

    private ModelAndView list(Request request, Response response) {
        SessionUser user = new SessionUser(request);

        return FTLUtil
                .getBaseModel("queries/list.ftl", request)
                .add("queries", service.listQueriesForUser(user.getId()).collect(Collectors.toList()));
    }
}
