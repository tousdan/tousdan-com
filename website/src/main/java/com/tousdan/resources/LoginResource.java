package com.tousdan.resources;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.tousdan.data.models.tables.records.UsersRecord;
import com.tousdan.services.UserPermissionsService;
import spark.Request;
import spark.Response;
import spark.Session;
import spark.TemplateEngine;

import java.util.Optional;

import static spark.Spark.post;

public class LoginResource implements Resource {
    private final Gson gson;

    private final UserPermissionsService permissions;
    private final TemplateEngine templateEngine;

    @Inject
    public LoginResource(TemplateEngine templateEngine, UserPermissionsService permissions) {
        this.gson = new Gson();
        this.permissions = permissions;
        this.templateEngine = templateEngine;
    }

    public void register() {
        post("/l", this::createSession, gson::toJson);
    }

    private Object createSession(Request req, Response res) {
        Optional<UsersRecord> user = permissions.authenticate(req.queryParams("user"), req.queryParams("pass"));

        if (user.isPresent()) {
            UsersRecord record = user.get();
            String role = record.getRole();

            Session session = req.session(true);

            session.attribute("role", role);
            session.attribute("authenticated", true);
            session.attribute("userId", record.getId());
            session.attribute("name", record.getName());

            String redirectUrl = req.queryParams("from");

            res.redirect(redirectUrl);
        } else {
            res.status(403);
        }

        return null;
    }

}
