package com.tousdan.messaging.exceptions;

/**
 * Created by tousdan on 22/07/15.
 */
public class MessageUndelivered extends RuntimeException {
    public MessageUndelivered(Throwable cause) {
        super(cause);
    }
}
