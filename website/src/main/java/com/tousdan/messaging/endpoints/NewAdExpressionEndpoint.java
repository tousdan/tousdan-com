package com.tousdan.messaging.endpoints;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.tousdan.messaging.JSONEndpoint;
import com.tousdan.messaging.MessagingEndpoint;
import com.tousdan.models.ads.AdExpression;
import com.tousdan.messaging.exceptions.MessageUndelivered;

import javax.inject.Inject;
import java.io.IOException;

public class NewAdExpressionEndpoint extends JSONEndpoint {

    @Inject
    public NewAdExpressionEndpoint(Gson serializer, Channel channel) {
        super(serializer, new QueueBuilder("scrape-expression")
                    .durable(true)
                    .autoDelete(false)
                    .exclusive(false), channel);
    }

    public void onNewAdExpression(AdExpression expression) {
        publish(expression);
    }
    public void onRefreshAdExpression(AdExpression expression) { publish(expression); }
}
