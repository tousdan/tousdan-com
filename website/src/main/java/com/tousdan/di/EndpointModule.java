package com.tousdan.di;

import com.google.inject.AbstractModule;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Created by tousdan on 22/07/15.
 */
public class EndpointModule extends AbstractModule {
    private final ConnectionFactory connectionFactory;

    public EndpointModule(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    protected void configure() {

        try {
            bind(Channel.class).toInstance(connectionFactory.newConnection().createChannel());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
