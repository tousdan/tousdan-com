package com.tousdan.di;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.tousdan.services.*;
import com.tousdan.services.ads.AdExpressionServiceImpl;
import com.tousdan.services.ads.AdsExpressionService;
import com.tousdan.services.queries.QueryDefinitionService;
import com.tousdan.services.queries.QueryDefinitionServiceImpl;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

public class DIModule extends AbstractModule {
    private static final Logger LOG = LoggerFactory.getLogger(DIModule.class);

    private final Configuration config;

    public DIModule() {
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        cpds.setJdbcUrl(System.getenv("DATABASE_URL"));

        String username = System.getenv("DATABASE_USER");
        String password = System.getenv("DATABASE_PASSWORD");

        if (username != null && username.length() > 0) { cpds.setUser(username); }
        if (password != null && password.length() > 0) { cpds.setPassword(password); }

        Configuration dbConfig = new DefaultConfiguration();
        dbConfig.set(SQLDialect.POSTGRES);
        dbConfig.set(cpds);

        this.config = dbConfig;
    }

    @Override
    protected void configure() {
        bind(TemplateEngine.class).to(FreeMarkerEngine.class).asEagerSingleton();

        bind(KeywordLinksService.class).to(KeywordLinksServiceImpl.class);
        bind(UserPermissionsService.class).to(UserPermissionsServiceImpl.class);
        bind(UserManagementService.class).to(UserManagementServiceImpl.class);

        bind(AdsExpressionService.class).to(AdExpressionServiceImpl.class);
        bind(QueryDefinitionService.class).to(QueryDefinitionServiceImpl.class);

        bind(DSLContext.class).toProvider(() -> DSL.using(config));

        bind(Gson.class).toInstance(new Gson());
    }


}