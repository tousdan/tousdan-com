package com.tousdan.services;

import com.tousdan.data.models.Tables;
import com.tousdan.data.models.tables.Users;
import com.tousdan.data.models.tables.records.UsersRecord;
import com.tousdan.exceptions.InsufficientRoleException;
import com.tousdan.exceptions.ValidationException;
import com.tousdan.models.Roles;
import com.tousdan.models.SessionUser;
import com.tousdan.models.User;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

public class UserManagementServiceImpl implements UserManagementService {
    private final DSLContext dsl;

    @Inject
    public UserManagementServiceImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public User addNewUser(SessionUser user, String name, Roles role) {
        if (!Roles.admin.equals(user.getRole())) {
            throw new InsufficientRoleException();
        }

        UsersRecord record = new UsersRecord();

        record.setName(name);
        record.setRole(role.name());

        try {
            dsl.executeInsert(record);
        } catch (DataAccessException dae) {
            throw new ValidationException("Could not add user", dae);
        }

        return new User(dsl.fetchOne(Users.USERS, Users.USERS.ID.eq(dsl.lastID().intValue())));
    }

    @Override
    public Collection<User> getUsers(SessionUser user) {
        if (!Roles.admin.equals(user.getRole())) {
            throw new InsufficientRoleException("Must be admin to list users");
        }

        return dsl.selectFrom(Users.USERS).fetch().map(User::new);
    }
}
