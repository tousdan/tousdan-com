package com.tousdan.services;

import com.tousdan.data.models.tables.records.UsersRecord;
import com.tousdan.models.Roles;
import com.tousdan.models.SessionUser;
import com.tousdan.models.User;
import spark.Session;

import java.util.Collection;

public interface UserManagementService {
    User addNewUser(SessionUser user, String name, Roles role);
    Collection<User> getUsers(SessionUser user);
}
