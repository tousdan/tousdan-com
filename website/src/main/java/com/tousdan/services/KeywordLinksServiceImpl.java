package com.tousdan.services;

import com.google.inject.Inject;
import com.tousdan.data.models.tables.records.KeywordLinksRecord;
import org.apache.commons.validator.routines.UrlValidator;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import static com.tousdan.data.models.Tables.KEYWORD_LINKS;

public class KeywordLinksServiceImpl implements KeywordLinksService {
    private final UrlValidator urlValidator;
    private final DSLContext dsl;

    @Inject
    public KeywordLinksServiceImpl(DSLContext dsl) {
        String[] schemes = {"http", "https"};
        this.urlValidator = new UrlValidator(schemes);

        this.dsl = dsl;
    }

    @Override
    public KeywordLinksRecord getKeyword(String keyword) {
        return dsl
            .selectFrom(KEYWORD_LINKS)
            .where(KEYWORD_LINKS.KEYWORD.eq(keyword))
            .fetchOne();
    }

    @Override
    public void addKeyword(String keyword, String url) throws DataAccessException {
        if (!urlValidator.isValid(url)) {
            throw new RuntimeException("Invalid keyword");
        }

        dsl.insertInto(KEYWORD_LINKS)
                .columns(KEYWORD_LINKS.KEYWORD, KEYWORD_LINKS.URL)
                .values(keyword, url)
                .execute();
    }
}
