package com.tousdan.services;

import com.tousdan.data.models.tables.records.KeywordLinksRecord;
import org.jooq.exception.DataAccessException;

public interface KeywordLinksService {
    KeywordLinksRecord getKeyword(String keyword);
    void addKeyword(String keyword, String url) throws DataAccessException;
}
