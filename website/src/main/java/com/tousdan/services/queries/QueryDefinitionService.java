package com.tousdan.services.queries;

import com.tousdan.models.queries.QueryDefinition;

import java.util.stream.Stream;

/**
 * Created by tousdan on 10/08/16.
 */
public interface QueryDefinitionService {
    Stream<QueryDefinition> listQueriesForUser(int userId);

    QueryDefinition addQuery(int userId, QueryDefinition query);
}
