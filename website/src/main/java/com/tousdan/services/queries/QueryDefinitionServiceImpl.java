package com.tousdan.services.queries;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tousdan.data.models.tables.records.AdExpressionsRecord;
import com.tousdan.data.models.tables.records.QueriesDefinitionRecord;
import com.tousdan.models.ads.AdExpression;
import com.tousdan.models.queries.QueryDefinition;
import com.tousdan.models.queries.QueryDefinitionBuilder;
import org.jooq.DSLContext;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.stream.Stream;

import static com.tousdan.data.models.Tables.QUERIES_DEFINITION;

public class QueryDefinitionServiceImpl implements QueryDefinitionService {
    private final DSLContext context;
    private final Gson gson;
    private final QueryDefinitionBuilder builder;

    @Inject
    public QueryDefinitionServiceImpl(DSLContext context, Gson gson, QueryDefinitionBuilder builder) {
        this.context = context;
        this.gson = gson;
        this.builder = builder;
    }

    @Override
    public Stream<QueryDefinition> listQueriesForUser(int userId) {
        return context.selectFrom(QUERIES_DEFINITION)
                .where(QUERIES_DEFINITION.USERID.eq(userId))
                .fetch()
                .map(builder::build)
                .stream();
    }

    @Override
    public QueryDefinition addQuery(int userId, QueryDefinition query) {
        JsonObject asJson = gson.toJsonTree(query).getAsJsonObject();

        asJson.remove("id");
        asJson.remove("type");

        QueriesDefinitionRecord record = new QueriesDefinitionRecord(
            null,
            query.getType().name(),
            asJson,
            null,
            userId
        );

        context.executeInsert(record);

        int id = context.lastID().intValue();

        record.setId(id);

        return builder.build(record);
    }
}
