package com.tousdan.services;

import com.tousdan.data.models.tables.Users;
import com.tousdan.data.models.tables.records.UsersRecord;
import com.tousdan.models.Roles;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;

import javax.inject.Inject;


import java.util.Optional;

import static com.tousdan.data.models.Tables.USERS;

public class UserPermissionsServiceImpl implements UserPermissionsService {

    private static final Logger LOG = LoggerFactory.getLogger(UserPermissionsServiceImpl.class);

    final DSLContext context;

    @Inject
    public UserPermissionsServiceImpl(DSLContext context) {
        this.context = context;
    }

    @Override
    public boolean isAuthenticated(Request req) {
        return Boolean.TRUE.equals(req.session().attribute("authenticated"));
    }

    @Override
    public Optional<UsersRecord> authenticate(String username, String password) {
        LOG.debug("Authenticating user :: %s", username);

        Optional<UsersRecord> userRecord = Optional.ofNullable(context.selectFrom(USERS)
                .where(USERS.NAME.equal(username))
                .fetchOne());

        if (!userRecord.isPresent()) {
            LOG.debug("Login failed for user :: %s", username);
        }

        return userRecord;
    }

    @Override
    public boolean hasRole(Request req, Roles role) {
        return role.name().equals(req.session().attribute("role"));
    }
}
