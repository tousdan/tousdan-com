package com.tousdan.services.ads;

import com.tousdan.models.SessionUser;
import com.tousdan.models.ads.AdAuthor;
import com.tousdan.models.ads.AdExpression;
import com.tousdan.models.ads.AdResult;

import java.util.Collection;

public interface AdsExpressionService {
    int addExpression(SessionUser user, AdExpression expression);
    AdExpression getExpression(int expression);
    Collection<AdExpression> listExpressions();

    Collection<AdResult> listResults(int expression);

    AdAuthor getAuthor(String authorId);

    void refresh(int expression);
}
