package com.tousdan.services.ads;

import static com.tousdan.data.models.Tables.*;

import com.rabbitmq.client.Connection;
import com.tousdan.data.models.tables.records.AdExpressionsRecord;
import com.tousdan.messaging.endpoints.NewAdExpressionEndpoint;
import com.tousdan.models.SessionUser;
import com.tousdan.models.ads.AdAuthor;
import com.tousdan.models.ads.AdExpression;
import com.tousdan.models.ads.AdResult;
import com.tousdan.models.ads.AdSite;
import org.jooq.DSLContext;

import javax.inject.Inject;
import java.util.Collection;

public class AdExpressionServiceImpl implements AdsExpressionService {
    private final DSLContext dsl;
    private final NewAdExpressionEndpoint messagingEndpoint;

    @Inject
    public AdExpressionServiceImpl(DSLContext dsl, NewAdExpressionEndpoint messagingEndpoint) {
        this.dsl = dsl;
        this.messagingEndpoint = messagingEndpoint;
    }

    @Override
    public int addExpression(SessionUser user, AdExpression expression) {
        AdExpressionsRecord record = new AdExpressionsRecord();

        record.setDescription(expression.getDescription());
        record.setQuery(expression.getQueryURL());
        record.setSite(AdSite.kijiji.name());
        record.setUserid(user.getId());

        dsl.executeInsert(record);

        int id = dsl.lastID().intValue();

        record.setId(id);

        messagingEndpoint.onNewAdExpression(new AdExpression(record));

        return id;
    }

    @Override
    public AdExpression getExpression(int expression) {
        AdExpressionsRecord record = dsl.selectFrom(AD_EXPRESSIONS)
            .where(AD_EXPRESSIONS.ID.eq(expression))
            .fetchOne();

        return new AdExpression(record);
    }

    @Override
    public Collection<AdExpression> listExpressions() {
         return dsl.selectFrom(AD_EXPRESSIONS)
             .orderBy(AD_EXPRESSIONS.CREATED.desc())
             .fetch()
             .map(AdExpression::new);
    }

    @Override
    public Collection<AdResult> listResults(int expression) {
        return dsl.selectFrom(AD_RESULTS)
                .where(AD_RESULTS.EXPRESSION.eq(expression))
                .fetch()
                .map(AdResult::new);
    }

    @Override
    public AdAuthor getAuthor(String authorId) {
        return new AdAuthor(dsl.selectFrom(AD_AUTHOR)
                .where(AD_AUTHOR.SITEID.eq(authorId))
                .fetchOne());
    }

    @Override
    public void refresh(int expression) {
        AdExpression expr = getExpression(expression);

        messagingEndpoint.onRefreshAdExpression(expr);
    }
}
