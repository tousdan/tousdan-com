package com.tousdan.services;

import com.tousdan.data.models.tables.records.UsersRecord;
import com.tousdan.models.Roles;
import spark.Request;

import java.util.Optional;

public interface UserPermissionsService {
    boolean isAuthenticated(Request req);
    boolean hasRole(Request req, Roles role);

    Optional<UsersRecord> authenticate(String username, String password);
}
