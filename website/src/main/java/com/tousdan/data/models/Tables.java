/**
 * This class is generated by jOOQ
 */
package com.tousdan.data.models;


import com.tousdan.data.models.tables.AdAuthor;
import com.tousdan.data.models.tables.AdExpressions;
import com.tousdan.data.models.tables.AdResults;
import com.tousdan.data.models.tables.KeywordLinks;
import com.tousdan.data.models.tables.QueriesDefinition;
import com.tousdan.data.models.tables.Users;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in public
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

	/**
	 * The table public.ad_author
	 */
	public static final AdAuthor AD_AUTHOR = com.tousdan.data.models.tables.AdAuthor.AD_AUTHOR;

	/**
	 * The table public.ad_expressions
	 */
	public static final AdExpressions AD_EXPRESSIONS = com.tousdan.data.models.tables.AdExpressions.AD_EXPRESSIONS;

	/**
	 * The table public.ad_results
	 */
	public static final AdResults AD_RESULTS = com.tousdan.data.models.tables.AdResults.AD_RESULTS;

	/**
	 * The table public.keyword_links
	 */
	public static final KeywordLinks KEYWORD_LINKS = com.tousdan.data.models.tables.KeywordLinks.KEYWORD_LINKS;

	/**
	 * The table public.queries_definition
	 */
	public static final QueriesDefinition QUERIES_DEFINITION = com.tousdan.data.models.tables.QueriesDefinition.QUERIES_DEFINITION;

	/**
	 * The table public.users
	 */
	public static final Users USERS = com.tousdan.data.models.tables.Users.USERS;
}
