package com.tousdan.util;

import com.tousdan.models.HTTPRequest;
import com.tousdan.models.SessionUser;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import spark.ModelAndView;
import spark.Request;
import spark.Session;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class  FTLUtil {

    private static class UrlBuilder implements TemplateMethodModel {
        private final String name;
        private final String base;

        public UrlBuilder(String name, String base) {
            this.name = name;
            this.base = base;
        }

        @Override
        public Object exec(List list) throws TemplateModelException {
            if (list.isEmpty()) {
                throw new TemplateModelException(name + " requires at least one argument - url");
            }

            String built = base;
            for(Object rawParam : list) {
                String param = rawParam.toString();

                if(param.startsWith("/")) {
                    built += param;
                } else {
                    built += "/" + param;
                }
            }

            return built;
        }
    }

    public static WebAppModelView getBaseModel(String template, Request request) {
        Map<String, Object> params = new HashMap<>();

        params.put("siteURL", new UrlBuilder("siteURL", ""));
        params.put("apiURL", new UrlBuilder("apiURL", "/api"));
        params.put("user", new SessionUser(request));
        params.put("request", HTTPRequest.fromRequest(request));

        return new WebAppModelView(params, template);
    }

    public static class WebAppModelView extends ModelAndView {
        public WebAppModelView(Map<String, Object> model, String viewName) {
            super(model, viewName);
        }

        public WebAppModelView add(String param, Object value) {
            ((Map) getModel()).put(param, value);

            return this;
        }
    }
}
