package com.tousdan.util;

import com.tousdan.models.Roles;
import com.tousdan.models.SessionUser;
import com.tousdan.services.UserPermissionsService;
import spark.Filter;
import spark.Route;
import spark.Spark;

public class RoutesUtil {

    public static Filter mustBeLoggedIn() {
        return (req, res) -> {
            if (!new SessionUser(req).isAuthenticated()) {
                res.redirect("/l?from=" + req.url());
                Spark.halt();
            }
        };
    }
    public static Filter failUnlessHasRole(final UserPermissionsService permissionsService, final Roles role) {
        return (req, res) -> {
            if (!permissionsService.hasRole(req, role)) {
                Spark.halt(403);
            }
        };
    }
}
