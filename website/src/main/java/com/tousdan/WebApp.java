package com.tousdan;

import com.google.common.collect.Lists;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.rabbitmq.client.*;
import com.tousdan.di.DIModule;
import com.tousdan.di.EndpointModule;
import com.tousdan.resources.*;
import com.tousdan.resources.ads.AdsCrawlerResource;
import com.tousdan.resources.api.ExpressionResource;
import com.tousdan.resources.api.JSONResource;
import com.tousdan.resources.api.QueryDefinitionResource;
import com.tousdan.resources.api.UsersResource;
import com.tousdan.resources.queries.QueriesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

import static spark.Spark.*;

public class WebApp
{
    private static final Logger LOGGER = LoggerFactory.getLogger(WebApp.class);

    List<Class<? extends Resource>> webResources = Lists.newArrayList(
        HomeResource.class,
        LoginResource.class,
        KeywordRedirectResource.class,
        AdminResource.class,
        AdsCrawlerResource.class,
        QueriesResource.class
    );

    List<Class<? extends JSONResource>> apiResources = Lists.newArrayList(
        UsersResource.class,
        ExpressionResource.class,
        QueryDefinitionResource.class
    );

    Injector injector;

    public static void main( String[] args ) throws Exception {
        int port = args.length > 0 ? Integer.parseInt(args[0]) : 4567;

        new WebApp(port);
    }

    public WebApp(int port) throws Exception {
        this.injector = Guice.createInjector(
                new DIModule(),
                new EndpointModule(configureRabbitConnectionFactory()));

        port(port);

        staticFileLocation("assets");

        initWebResources();
        initAPIResources();

    }

    private void initAPIResources() {
        apiResources.stream()
                .map(this.injector::getInstance)
                .forEach(wri -> {
                    LOGGER.info("Registering API {}", wri);
                    wri.register();
                });
    }

    private void initWebResources() {
        webResources.stream()
                .map(this.injector::getInstance)
                .forEach(wri -> {
                    LOGGER.info("Registering {}", wri);
                    wri.register();
                });
    }

    private ConnectionFactory configureRabbitConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("AMQP_HOST"));
        factory.setAutomaticRecoveryEnabled(true);
        return factory;
    }
}
