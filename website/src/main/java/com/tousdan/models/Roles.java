package com.tousdan.models;

public enum Roles {
    admin,
    manage,
    view
}
