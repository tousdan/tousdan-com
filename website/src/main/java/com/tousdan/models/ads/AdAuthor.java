package com.tousdan.models.ads;

import com.tousdan.data.models.tables.records.AdAuthorRecord;

public class AdAuthor {
    private final long id;
    private final String siteId;

    public AdAuthor(long id, String siteId) {
        this.id = id;
        this.siteId = siteId;
    }

    public AdAuthor(AdAuthorRecord record) {
        this.id = record.getId();
        this.siteId = record.getSiteid();
    }

    public long getId() {
        return id;
    }

    public String getSiteId() {
        return siteId;
    }
}
