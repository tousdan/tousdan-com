package com.tousdan.models.ads;

import com.tousdan.data.models.tables.records.AdResultsRecord;

import java.sql.Timestamp;

public class AdResult {
    private final long id;
    private final Timestamp fetchTime;
    private final String title;
    private final String URL;
    private final Integer price;

    public AdResult(long id, Timestamp fetchTime, String URL, String title, int price) {
        this.id = id;
        this.title = title;
        this.fetchTime = fetchTime;
        this.URL = URL;
        this.price = price;
    }

    public AdResult(AdResultsRecord record) {
        this.id = record.getId();
        this.title = record.getTitle();
        this.fetchTime = record.getFetchedon();
        this.URL = record.getUrl();
        this.price = record.getPrice();
    }

    public long getId() {
        return id;
    }

    public Timestamp getFetchTime() {
        return fetchTime;
    }

    public String getURL() {
        return URL;
    }

    public Integer getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        //TODO: Implement me.
        return title;
    }

    public String getDetail() {
        //TODO: Implement me.
        return title;
    }

    public String getImageURL() {
        return "http://loremflickr.com/200/160";
    }
}
