package com.tousdan.models.ads;

import com.tousdan.data.models.tables.records.AdExpressionsRecord;

public class AdExpression {
    private final Integer id;
    private final AdSite site;
    private final String description;
    private final String queryURL;

    public AdExpression(Integer id, AdSite site, String description, String queryURL) {
        this.id = id;
        this.site = site;
        this.description = description;
        this.queryURL = queryURL;
    }

    public AdExpression(AdExpressionsRecord record) {
        this.id = record.getId();
        this.site = AdSite.valueOf(record.getSite());
        this.description = record.getDescription();
        this.queryURL = record.getQuery();
    }

    public Integer getId() {
        return id;
    }

    public AdSite getSite() {
        return site;
    }

    public String getDescription() {
        return description;
    }

    public String getQueryURL() {
        return queryURL;
    }
}
