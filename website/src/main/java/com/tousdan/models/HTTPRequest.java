package com.tousdan.models;

import spark.Request;

/**
 * Created by tousdan on 06/04/16.
 */
public class HTTPRequest {

    private final String currentURL;
    private final String pathInfo;

    private HTTPRequest(String currentURL, String pathInfo) {
        this.currentURL = currentURL;
        this.pathInfo = pathInfo;
    }

    public static HTTPRequest fromRequest(Request request) {
        return new HTTPRequest(request.url(), request.pathInfo());
    }

    public String getCurrentURL() {
        return currentURL;
    }

    public String getPathInfo() {
        return pathInfo;
    }
}
