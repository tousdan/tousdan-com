package com.tousdan.models;

import spark.Request;

public class SessionUser {
    private final boolean authenticated;
    private final Roles role;
    private final Integer id;
    private final String name;

    public SessionUser(Request request) {
        this.authenticated = Boolean.TRUE.equals(request.session().attribute("authenticated"));

        if (this.authenticated) {
            this.role = Roles.valueOf(request.session().attribute("role"));
            this.id = request.session().attribute("userId");
            this.name = request.session().attribute("name");
        } else {
            this.role = null;
            this.id = null;
            this.name = "";
        }
    }

    public Roles getRole() {
        return role;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public Integer getId() { return id; }

    public String getName() {
        return name;
    }

    public boolean isAdmin() {
        return Roles.admin.equals(this.role);
    }
}
