package com.tousdan.models.queries;

import com.google.gson.JsonObject;
import com.tousdan.data.models.tables.records.QueriesDefinitionRecord;

/**
 * Created by tousdan on 10/08/16.
 */
public class GamePriceQueryDefinition implements QueryDefinition {
    @Override
    public QueryType getType() {
        return QueryType.GamePrice;
    }

    public String getName() {
        return name;
    }

    public long getSteamAppId() {
        return steamAppId;
    }

    public GamePriceQueryDefinition(QueriesDefinitionRecord record) {
        this(record.getQuery().getAsJsonObject().get("name").getAsString());

        JsonObject obj = record.getQuery().getAsJsonObject();

        this.id = record.getId();
        this.steamAppId = !obj.has("steamAppId") ? -1 : obj.get("steamAppId").getAsLong();
    }

    public GamePriceQueryDefinition(String name) {
        this.name = name;
    }

    @Override
    public int getId() {
        return id;
    }

    private int id;
    private String name;
    private long steamAppId;
}

