package com.tousdan.models.queries;

import com.google.gson.Gson;
import com.tousdan.data.models.tables.records.QueriesDefinitionRecord;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.inject.Inject;

/**
 * Created by tousdan on 10/08/16.
 */
public class QueryDefinitionBuilder {
    private final Gson gson;

    @Inject
    public QueryDefinitionBuilder(Gson gson) {
        this.gson = gson;
    }

    public QueryDefinition build(QueriesDefinitionRecord record) {
        QueryDefinition def = new GamePriceQueryDefinition(record);

        return def;
    }
}
