package com.tousdan.models.queries;

public interface QueryDefinition {
    public enum QueryType {
        GamePrice
    }

    int getId();
    QueryType getType();
}
