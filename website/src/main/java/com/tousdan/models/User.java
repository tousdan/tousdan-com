package com.tousdan.models;

import com.tousdan.data.models.tables.records.UsersRecord;

public class User {
    private final String name;
    private final Roles role;

    public User(String name, Roles role) {
        this.name = name;
        this.role = role;
    }

    public User(UsersRecord record) {
        this(record.getName(), Roles.valueOf(record.getRole()));
    }

    public String getName() {
        return name;
    }

    public Roles getRole() {
        return role;
    }
}
