package com.tousdan.exceptions;

public class InsufficientRoleException extends RuntimeException {
    public InsufficientRoleException() {
    }

    public InsufficientRoleException(String message) {
        super(message);
    }
}
