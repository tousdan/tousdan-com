(function($, Backbone, Marionette, _, tpl) {
    var views = {},
        models = {};

    models.Query = Backbone.Model.extend({
    });

    models.Queries = Backbone.Collection.extend({
        url: '/api/queries',
        model: models.Query

    });

    var app = new Marionette.Application({
    });

    app.on('start', function() {
        var queries = new models.Queries();

        queries.fetch();
    });

    return app;
})($, Backbone, Backbone.Marionette, _, {
    get: function(name) {
      return _.template("hello");
    }
}).start();