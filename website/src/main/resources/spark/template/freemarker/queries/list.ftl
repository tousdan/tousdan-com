<#include "/base.ftl">

<#macro head>
<title>Queries</title>
</#macro>

<#macro body>
    <h1>Queries</h1>
    <#list queries as query>
        <p>
            <span>${query.id}</span>
        </p>
    </#list>
    <a id="add-new-query" href="#">Add a new query</a>
</#macro>

<#macro includes>
    <script src="/site/js/queries.js"> </script>
</#macro>

<@show />