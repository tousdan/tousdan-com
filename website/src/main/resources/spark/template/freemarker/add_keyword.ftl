<#include "base.ftl">

<#macro head>
<title>Add a keyword redirect</title>
</#macro>

<#macro body>
<h1>Add a keyword</h1>
<form method="post">
<div>
    <label for="keyword">Keyword</label>
    <input type="text" name="keyword" />
</div>
<div>
    <label for="url">URL</label>
    <input type="text" name="url>
</div>
 <div class="button">
    <button type="submit">Add</button>
</div>
</form>
</#macro>
<@show />