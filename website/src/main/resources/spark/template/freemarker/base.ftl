<#macro head>
</#macro>

<#macro body>
</#macro>

<#macro includes>
</#macro>

<#macro navbar_links>
  <#if user?? && user.id??>
    <li><a href="/ads/expressions">Expressions</a></li>
    <li><a href="/queries/list">Queries</a></li>
  </#if>
</#macro>

<#macro show>
<!DOCTYPE html>
<html lang="en">
  <head>
    <@head />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/site/css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Ads scraper</a>
          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="navbar-nav nav">
              <@navbar_links />
            </ul>
            <div class="navbar-right">
              <#if user?? && user.id?? >
                <p id="login-status" class="navbar-text">Welcome <strong>${user.name}</strong> <#if user.admin ><a href="/a">(Admin)</a></#if></p>
              <#else>
                <form class="navbar-form" method="POST" action="/l?from=${request.currentURL}">
                  <div class="form-group">
                    <label class="sr-only" for="user">Username</label>
                    <input type="text" class="form-control" id="user" name="user" placeholder="Username">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="pass">Password</label>
                    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
                  </div>
                  <button type="submit" class="btn btn-default">Login</button>
                </form>
              </#if>
            </div>
          </div>
        </div>
      </nav>

      <div class="container-fluid">
        <div class="row">
          <div class="main">
            <@body />
          </div>
        </div>
      </div>

      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="/site/3rd/jquery.js"></script>
      <script src="/site/3rd/json2.js"></script>
      <script src="/site/3rd/underscore.js"></script>
      <script src="/site/3rd/backbone.js"></script>
      <script src="/site/3rd/backbone.wreqr.js"></script>
      <script src="/site/3rd/backbone.babysitter.js"></script>
      <script src="/site/3rd/backbone.marionette.js"></script>
      <script src="/bootstrap/js/bootstrap.min.js"></script>

      <@includes />
    </body>
  </html>
</#macro>
