<#include "/base.ftl">

<#macro head>
<title>Users</title>
</#macro>

<#macro body>
    <div class="panel panel-default">
        <div class="panel-heading">User management</div>
        <div class="panel-body">
            <a href="${siteURL("a/users")}">Add users</a>
        </div>
    </div>
</#macro>

<@show />