<#include "/base.ftl">

<#macro head>
<title>Users</title>
</#macro>

<#macro body>
    <h1>Users</h1>
    <#list users as user>
        <div>
            <span>${user.name}</span>
            <span>${user.role}</span>
        </div>
    </#list>
    <a href="${siteURL("a/users/new")}">Add user</a>
</#macro>

<@show />