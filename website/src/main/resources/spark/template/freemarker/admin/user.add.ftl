<#include "/base.ftl">

<#macro head>
<title>Add a new user</title>
</#macro>

<#macro body>
    <h1>Add a new user</h1>
            <form method="POST" action="${apiURL("users")}">
                <div>
                    <label for="user">Name:</label>
                    <input type="text" name="user" />
                </div>
                <div>
                    <label for="role">Role:</label>
                    <select name="role">
                        <#list roles as role>
                            <option value="${role}">${role}</option>
                        </#list>
                    </select>
                </div>

                <div class="button">
                    <button type="submit">Create</button>
                </div>
            </form>
</#macro>

<@show />