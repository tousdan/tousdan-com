<#include "/base.ftl">

<#macro head>
<title>Expressions</title>
</#macro>

<#macro body>
    <h1>Expressions</h1>
    <#list expressions as expr>
        <p>
            <a href="${siteURL("ads/expressions", expr.id)}">
                <span>${expr.description}</span>
            </a>
            <span>${expr.queryURL}</span>
        </p>
    </#list>
    <a href="${siteURL("ads/expressions/new")}">Add a new expressions</a>
</#macro>

<@show />