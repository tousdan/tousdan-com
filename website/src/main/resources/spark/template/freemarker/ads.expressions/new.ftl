<#include "/base.ftl">

<#macro head>
<title>Add a new expressions</title>
</#macro>

<#macro body>
    <form method="post" action="${apiURL("expressions")}">
        <input type="hidden" name="expr[site]" value="kijiji" />
        <div class="form-group">
            <label for="description">Description</label>
            <input id="description" type="text" name="expr[description]" class="form-control" />
        </div>
        <div class="form-group">
            <label for="url">URL</label>
            <input id="url" type="text" name="expr[query]" class="form-control">
        </div>
         <div class="button">
            <button type="submit" class="btn btn-default">Add an new expression</button>
        </div>
    </form>
</#macro>

<@show />