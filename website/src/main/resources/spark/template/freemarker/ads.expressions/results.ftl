<#include "/base.ftl">

<#macro head>
<title>${expression.description}</title>
</#macro>

<#macro body>
    <h1>Latest results for expression; ${expression.description}</h1>

    <#list results?chunk(3) as row>
        <div class="row">
        <#list row as result>
        <div class="col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <a href="${result.URL}">
                        <h4>${result.title}</h4>
                        <img src="${result.imageURL}" />
                    </a>
                    <p>
                        ${result.summary}
                    </p>

                    <#if result.price??>
                    <p><span>${result.price} $</span></p>
                    </#if>

                    <div class="btn-group btn-group-justified" role="group">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-success">Interesting</button>
                        </div>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Not interesting
                                <span class="carent"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Price too high</a></li>
                                <li><a href="#">Irrelevant to search</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p-->
                </div>
            </div>
        </div>
        </#list>
        </div>
    </#list>
</#macro>

<@show />