To generate a pg db;
	pg_ctl initdb -D vendor/postgresql

To regenerate models from the database schema;
    mvn clean package
    java $JAVA_OPTS -cp 'target/classes:target/dependency/*' org.jooq.util.GenerationTool target/classes/library.xml

To launch web app
    see command in Procfile, under web task.