CREATE TABLE keyword_links(
    keyword text primary key not null,
    url text,
    created timestamp with time zone default current_timestamp
);