CREATE TABLE queries_definition(
    id SERIAL primary key,
    type text not null,
    query json not null,
    created timestamp with time zone default current_timestamp,
    userid integer not null references users(id)
);


