CREATE TABLE users(
    id SERIAL primary key,
    name text not null unique,
    role text not null,
    created timestamp with time zone default current_timestamp
);