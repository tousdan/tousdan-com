CREATE TABLE ad_author(
    id SERIAL primary key,
    siteId text not null unique
);

CREATE TABLE ad_expressions(
    id SERIAL primary key,
    site text not null,
    description text not null,
    query text not null,
    created timestamp with time zone default current_timestamp,
    userid integer not null references users(id)
);

CREATE TABLE ad_results(
    id SERIAL primary key,
    expression integer not null references ad_expressions(id),
    author integer not null references ad_author(id),
    fetchedOn timestamp with time zone default current_timestamp,
    url text not null,
    price integer
);